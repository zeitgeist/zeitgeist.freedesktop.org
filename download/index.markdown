---
layout: default
title: Download
---

# Download

## Source Release

 * [You can download the latest version of Zeitgeist (1.0.3) from GitLab](https://gitlab.freedesktop.org/zeitgeist/zeitgeist/tags/v1.0.3)
 * [See older releases](https://gitlab.freedesktop.org/zeitgeist/zeitgeist/tags)

## Development Version

Go to the [development page](/development/) for more information.
