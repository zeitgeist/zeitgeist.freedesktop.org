---
layout: default
title: Documentation
---

# Documentation

The documentation is generated using [Gtk-Doc](https://www.gtk.org/gtk-doc/), it is available for C and Vala.
For Vala documentation, prefer the [valadoc.org Website](https://valadoc.org/) that is easier to navigate.

## Latest Release

 * [Zeitgeist 1.0.3 C API Documentation](http://zeitgeist.freedesktop.org/documentation/latest/C/index.html)
 * [Zeitgeist 1.0.3 Vala API Documentation](http://zeitgeist.freedesktop.org/documentation/latest/Vala/index.html)
