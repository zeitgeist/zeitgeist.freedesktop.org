ZeitGeist Website
=================

This is a tiny website to introduce to the Zeitgeist library.

License
---------

GPLv2 or higher

Based on https://github.com/redwallhp/solar-theme-jekyll
