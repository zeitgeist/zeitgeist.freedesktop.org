---
layout: default
title: Development
---

# Development

The code is hosted in the [freedesktop.org GitLab instance](https://gitlab.freedesktop.org/zeitgeist).

You can clone the repository using this command:

`git clone https://gitlab.freedesktop.org/zeitgeist/zeitgeist.git`

To submit a change, use a fork of the Git repository and submit a pull request with the GitLab interface.

You can link the issue your pull request is fixing by adding `fixes #XXX` (XXX being the number of the issue) to the description of your pull request.
